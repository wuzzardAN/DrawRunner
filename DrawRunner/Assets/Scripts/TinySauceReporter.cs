﻿#if UNITY_EDITOR
using UnityEditor;
#endif

using UnityEngine;

[CreateAssetMenu(menuName = "Framework/Analytics/TinySauceReporter")]
public class TinySauceReporter : ScriptableObject
{



    public void OnGameStart()
    {
#if GAMEANALYTICS
        TinySauce.OnGameStarted($"Level" + GameManager.level);
#endif
    }

    public void OnGameLost()
    {
#if GAMEANALYTICS
        TinySauce.OnGameFinished($"Level" + GameManager.level,0);
#endif

    }

    public void OnGameWon()
    {
#if GAMEANALYTICS
        TinySauce.OnGameFinished($"Level" + GameManager.level,0);
#endif

    }
}