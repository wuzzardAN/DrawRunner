﻿using UnityEngine;

public class AnalyticsEventHandler : MonoBehaviour
{
    public static AnalyticsEventHandler Instance;
    [SerializeField] private TinySauceReporter reporter;
    private void Start()
    {
        if (Instance == null) Instance = this;
    }

    public void OnGameWon()
    {
        reporter.OnGameWon();
    }

    public void OnGameLost()
    {
        reporter.OnGameLost();
    }

    public void OnGameStart()
    {
        reporter.OnGameStart();
    }
}
